/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tarea.pkg2.david.vega.santamaria;

import java.util.Scanner;

/**
 *
 * @author Admin
 */
public class Salarios {
    
    
    public static void salarios() {

        Scanner var = new Scanner(System.in);
        System.out.println("-------------------------------");
        System.out.println("Digite la cantidad de horas laboradas en el mes: ");
        float horas_laboradas = var.nextInt();
        System.out.println("-------------------------------");
        System.out.println("Digite el precio por hora: ");
        float precio_hora = var.nextInt();
        System.out.println("-------------------------------");

        float salarioBruto = horas_laboradas * precio_hora;
        float salarioNeto = (float) (salarioBruto - (salarioBruto * (9.17 / 100)));
        float deducciones = salarioBruto - salarioNeto;

        System.out.println("El salario bruto es de: " + salarioBruto + "El salario neto es de: " + salarioNeto + "Las deducciones son de: " + deducciones);

    }
    
}
