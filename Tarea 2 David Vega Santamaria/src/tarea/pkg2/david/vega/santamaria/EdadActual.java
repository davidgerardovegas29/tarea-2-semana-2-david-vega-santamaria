/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tarea.pkg2.david.vega.santamaria;

import java.util.Scanner;

/**
 *
 * @author Admin
 */
public class EdadActual {

    public static void edadactual() {

        Scanner var = new Scanner(System.in);
        System.out.println("-------------------------------");
        System.out.println("Ingrese su año de nacimiento: ");
        int año_nacimiento = var.nextInt();
        System.out.println("-------------------------------");
        System.out.println("Ingrese el año actual: ");
        System.out.println("-------------------------------");
        int año_actual = var.nextInt();

        int edad = año_actual - año_nacimiento;

        Scanner var1 = new Scanner(System.in);
        System.out.println("¿Usted ya cumplió años? Si/No : ");
        String cumplio_años = var1.nextLine();

        if (("Si".equals(cumplio_años)) || ("si".equals(cumplio_años))) {
            System.out.println("Su edad es: " + edad);
        } else if (("No".equals(cumplio_años)) || ("no".equals(cumplio_años))) {
            edad = edad - 1;
            System.out.println("Su edad es: " + edad);
        }

    }
}
